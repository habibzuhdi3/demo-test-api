package com.domain.models.repos;

import com.domain.models.entities.Terserah;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface repository extends JpaRepository<Terserah, Integer>{

}
